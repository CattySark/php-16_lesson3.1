1. Инкапсуляция, это когда часть кода спрятана внутри какого-нибудь компонента.
2. Как говориться:'наши недостатки - это продолжения наших достоинств!'.)
 Использовать объекты легче, а создавать сложнее. Если объект наследник, то кода требуется меньше,
 но необходими следить за изменением кода родителей, чтобы не навредить наследникам.
 
class Cars
{
  public $color=black;
  public $power=100;

  public function getColor(){
	   return $this->color;
  }
  
}
$BMW=new Cars;
$opel=new Cars;

class TV
{
  public $brand;
 
  public function getbrand(){
	   return $this->brand;
  }
  
}
$TV1=new TV;
$TV1->brand='LG';
$TV2=new TV;
$TV2->brand='Samsung';

class Ballpoint
{
 public $color='blue';
 public $use=0;
 
  public function BeUsed(){
	 $use=1;
  }
}

$pen=new ballpoint;
$pensil=new ballpoint;

class Duck
{
  public $country();
  
  public function getCountry(){
	   return $this->country;
  }
}
$DonaldDuck=new Duck;
$DonaldDuck->country='Америка';
$uglyDuck=new Duck;
$uglyDuck->country='Россия';

class Product
{
  public $price;
  public $name;
  
  public function_construct($name,$price)
   {
	   $this->name=$name;
	   $this->price=$price;
   }
  
}
$table=new Product('стол',10 000);
$chair=new Product('стул',500);
